angular.module('yoeman.directives', [])
  .directive('liveGraph', function() {
    return {
      restrict: 'E',
      template: '<div></div>',
      transclude: true,
      replace: true,

      link: function(scope, element, attrs) {
        var chartsDefaults = {
          chart: {
            renderTo: element[0],
            type: "spline",
            height: attrs.height || 500,
            width: attrs.width || null,
            animation: true,
            backgroundColor: 'rgba(255, 255, 255, 0)'
          },
          credits: {
            enabled: false
          },
          plotOptions: {
            column: {
              groupPadding: 0,
              pointPadding: 0.2,
              borderWidth: 0
            },
            series: {
              animation: {
                duration: 300,
                easing: 'linear'
              },

            }
          },
          legend: {
            borderWidth: 0,
            itemStyle: {
              color: '#111',
              fontSize: '25px'
            }
          },
          yAxis: {
            labels: {
              enabled: true
            },
            title: {
              text: null
            },
            gridLineWidth: 0
          },
          xAxis: {
            labels: {
              enabled: false
            },
            tickLength: 0,
            title: {
              text: null
            },
            lineColor: '#3F3F3F'
          },
          title: {
            text: null,
            floating: true
          },
          series: []
        };

        // Set chart object
        var chart = new Highcharts.Chart(chartsDefaults);
        //console.log(JSON.stringify(chartsDefaults));
        //Update charts data initially and when it changes
        scope.$watch(function() {
          return scope.stuff;
        }, function(value) {
          var data = scope.stuff;
          console.log(JSON.stringify(data));
          if (shouldAddSeries(chart, data)) {
            chart.addSeries(data[0]);
          }

          function shouldAddSeries(chart, data) {
            var hasSeries = false;
            _.each(chart.series, function(chartSeries) {
              console.log(JSON.stringify(chart.series[0].name));
              if (data.name == chartSeries.name) {
                hasSeries = true;
              }
            });
            return !hasSeries;
          }


        }, true);
      }
    }
  });