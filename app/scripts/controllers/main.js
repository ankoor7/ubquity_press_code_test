'use strict';

angular.module('yoemanApp')
  .controller('MainCtrl', function($scope, _, $window) {
    var list = {},
      source = testData;
    list = _.chain(source[0].fields).map(function(value, key) {
      if (key.indexOf('_diff') === -1 && !_.contains(['state_date', 'article'], key)) {
        var change = _.last(source).fields[key] - value;
        change = (change > 0) ? '+' + change.toString() : change.toString();
        return {
          name: key,
          diff: change
        };
      }
    }).compact().value();

    $scope.list = list;

    $scope.toggle = function($event, name) {
      $scope.stuff = extract(name, source);
    }

    function extract(key, source) {
      if (_.has(source[0].fields, key)) {
        var data = [];
        _.each(source, function(item) {
          var point = [Date.parse(item.fields.state_date), item.fields[key]];
          data.push(point);
        });
        return [{
          name: key,
          data: data
        }]
      } else {
        throw new Error('Key ' + key + ' is not a valied data field.');
      }
    }
  });