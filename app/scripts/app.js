'use strict';

angular
  .module('yoemanApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'yoeman.directives',
    'underscore'
  ])
  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('contact', {
        templateUrl: 'views/contact.html'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

var underscore = angular.module('underscore', []);
underscore.factory('_', function() {
  return window._;
});